import sqlite3
import datetime
import numpy as np

connection = sqlite3.connect("results.db")
cursor = connection.cursor()
cursor.execute("SELECT * FROM temperature")

result = cursor.fetchall()
#print(result)

dates = []
values = []

for r in result:
    dates.append(datetime.datetime.strptime(r[0], "%Y-%m-%dT%H:%M:%S"))
    values.append(r[1])

# print(dates, values)

current = dates[0].date()
acc = 0
count = 0

msg = []

low = (dates[0], values[0])
high = (dates[0], values[0])

for i in np.arange(0, len(dates)):
    if current != dates[i].date():
        msg.append("Avg. temp for " + str(current) + " was " + str(acc / count))
        current = dates[i].date()
        acc = 0
        count = 0
        # print("----- new day -----")

    if values[i] < low[1]:
        low = (dates[i], values[i])
    if values[i] > high[1]:
        high = (dates[i], values[i])

    acc += values[i]
    count = count + 1
    # print(acc / count)
msg.append("Avg. temp for " + str(current) + " was " + str(acc / count))
for sz in msg:
    print(sz)
print("Absolute high was on " + str(high[0]) + " with " + str(high[1]))
print("Absolute low was on " + str(low[0]) + " with " + str(low[1]))