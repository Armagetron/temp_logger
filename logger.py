#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sqlite3
import sys
import time
import glob
import datetime

# global variables
speriod = 10
dbname = '/home/pi/templog.db'


# store the temperature in the database
def log_temperature(temp):
    conn = sqlite3.connect(dbname)
    curs = conn.cursor()

    try:
        curs.execute("INSERT INTO temperature values((?), (?))",
                     (datetime.datetime.now().replace(microsecond=0).isoformat(), temp,))
    except sqlite3.IntegrityError:
        print("sqlite3 Integrity Error")
        # most likely we read too often

    # commit the changes
    conn.commit()

    conn.close()


# display the contents of the database
def display_data():
    conn = sqlite3.connect(dbname)
    curs = conn.cursor()

    for row in curs.execute("SELECT * FROM temperature"):
        print(str(row[0]) + "	" + str(row[1]))

    conn.close()


def read_temp_sensor(sensor_name):
    """Aus dem Systembus lese ich die Temperatur der DS18B20 aus."""
    f = open(sensor_name, 'r')
    lines = f.readlines()
    f.close()
    return lines


def read_temp_lines(sensor_name):
    lines = read_temp_sensor(sensor_name)
    # wait for the data to be available
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_sensor(sensor_name)
    temperatur_str = lines[1].find('t=')
    # check the data
    if temperatur_str != -1:
        temp_data = lines[1][temperatur_str + 2:]
        temp_celsius = float(temp_data) / 1000.0
        # tempKelvin = 273 + float(tempData) / 1000
        return temp_celsius


def main():
    # search for a device file that starts with 28
    # auto-discovery for temperature sensors
    devicelist = glob.glob('/sys/bus/w1/devices/28*')
    if devicelist == '':
        return None
    else:
        # append /w1slave to the device file
        w1devicefile = devicelist[0] + '/w1_slave'

    try:
        while True:
            temperature = read_temp_lines(w1devicefile)
            print("Temperature at " + time.strftime('%H:%M:%S') + " inside: " + str(temperature) + " °C")
            log_temperature(temperature)
            time.sleep(speriod)
    except KeyboardInterrupt:
        # Graceful terminate at CTRL+C
        print('Measurement terminated')
    except Exception as e:
        print(str(e))
        sys.exit(1)
    finally:
        print('Programm terminated.')
        sys.exit(0)


if __name__ == "__main__":
    main()
